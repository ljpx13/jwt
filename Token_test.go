package jwt

import (
	"crypto/ecdsa"
	"crypto/elliptic"
	"crypto/rand"
	"crypto/x509"
	"encoding/pem"
	"fmt"
	"testing"

	"gitlab.com/ljpx13/test"
	"gitlab.com/ljpx13/test/verbs/is"
)

func TestTokenSerializeAndParse(t *testing.T) {
	// Arrange.
	token1 := &Token{
		Header: Header{Algorithm: ES256, Type: "JWT"},
		Body: map[string]interface{}{
			"Key1": "Value1",
			"Key2": "Value2",
		},
		Signature: []byte("Hello, World!"),
	}

	// Act.
	tokenString, err := token1.Serialize()
	test.That(t, err, is.Nil())

	token2, err := Parse(tokenString)
	test.That(t, err, is.Nil())

	// Assert.
	test.That(t, token2.Header.Algorithm, is.EqualTo(ES256))
	test.That(t, token2.Body["Key1"], is.EqualTo("Value1"))
	test.That(t, token2.Body["Key2"], is.EqualTo("Value2"))
	test.That(t, string(token2.Signature), is.EqualTo("Hello, World!"))
}

func TestTokenScopes(t *testing.T) {
	// Arrange.
	token := NewToken()

	// Act.
	token.AddScope("user:create")
	token.AddScope("user:delete")

	hasDelete := token.HasScope("user:delete")
	hasRead := token.HasScope("user:read")

	token.RemoveScope("user:delete")

	// Assert.
	test.That(t, hasDelete, is.True())
	test.That(t, hasRead, is.False())

	scopes, ok := token.Body["scope"].([]string)
	test.That(t, ok, is.True())
	test.That(t, scopes[0], is.EqualTo("user:create"))
}

func TestTokenScopesImmutableWhenSigned(t *testing.T) {
	// Arrange.
	privateKey, err := ecdsa.GenerateKey(elliptic.P256(), rand.Reader)
	test.That(t, err, is.Nil())

	signer := NewES256Signer(privateKey)

	token := NewToken()
	token.AddScope("user:create")

	err = token.Sign(signer)
	test.That(t, err, is.Nil())
	test.That(t, token.IsSigned(), is.True())

	// Assert.
	token.AddScope("user:delete")
	token.RemoveScope("user:create")

	test.That(t, token.HasScope("user:delete"), is.False())
	test.That(t, token.HasScope("user:create"), is.True())
}

func TestTokenAddClaimCannotInteractWithScope(t *testing.T) {
	// Arrange.
	token := NewToken()

	// Act.
	token.AddClaim("scope", []string{"user:create"})

	// Assert.
	test.That(t, token.HasScope("user:create"), is.False())
}

func TestTokenRemoveClaimCannotInteractWithScope(t *testing.T) {
	// Arrange.
	token := NewToken()

	// Act.
	token.AddScope("user:delete")
	token.RemoveClaim("scope")

	// Assert.
	test.That(t, token.HasScope("user:delete"), is.True())
}

func TestTokenGetClaimCannotGetScopes(t *testing.T) {
	// Arrange.
	token := NewToken()

	// Act.
	token.AddScope("user:read")

	// Assert.
	value, ok := token.GetClaim("scope")
	test.That(t, value, is.Nil())
	test.That(t, ok, is.False())
}

func TestTokenClaims(t *testing.T) {
	// Arrange.
	token := NewToken()

	// Act.
	token.AddClaim("iss", "Test Issuer")
	token.AddClaim("exp", 86400)
	token.RemoveClaim("exp")

	// Assert.
	iss, ok := token.GetStringClaim("iss")
	test.That(t, ok, is.True())
	test.That(t, iss, is.EqualTo("Test Issuer"))

	exp, ok := token.GetClaim("exp")
	test.That(t, ok, is.False())
	test.That(t, exp, is.Nil())
}

func TestTokenE2E(t *testing.T) {
	// Arrange.
	privateKey, err := ecdsa.GenerateKey(elliptic.P256(), rand.Reader)
	test.That(t, err, is.Nil())

	publicKey := &privateKey.PublicKey

	signer := NewES256Signer(privateKey)
	verifier := NewES256Verifier(publicKey)

	token := NewToken()
	token.AddClaim("iss", "Test Issuer")
	token.AddScope("user:create")
	token.AddScope("user:delete")

	// Act.
	err = token.Sign(signer)
	test.That(t, err, is.Nil())

	valid := token.Verify(verifier)

	// Output.
	serialized, err := token.Serialize()
	test.That(t, err, is.Nil())

	rawPublicKey, err := x509.MarshalPKIXPublicKey(publicKey)
	test.That(t, err, is.Nil())

	pemBlock := &pem.Block{
		Type:  "PUBLIC KEY",
		Bytes: rawPublicKey,
	}

	pem := string(pem.EncodeToMemory(pemBlock))

	fmt.Printf("\n\n%v\n\n", pem)
	fmt.Printf("%v\n\n", serialized)

	// Assert.
	test.That(t, valid, is.True())
}
